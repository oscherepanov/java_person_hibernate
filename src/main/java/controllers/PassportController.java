package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import models.Passport;

public class PassportController {
    @FXML
    private TextField seriesEdit;

    @FXML
    private TextField numberEdit;

    private Passport passport;

    public void setPassport(Passport passport) {
        this.passport = passport;
    }

    public void toView() {
        if (passport != null) {
            seriesEdit.setText(passport.getSeries());
            numberEdit.setText(passport.getNumber());
        }
    }

    public void fromView() {
        if (passport != null) {
            passport.setSeries(seriesEdit.getText());
            passport.setNumber(numberEdit.getText());
        }
    }
}

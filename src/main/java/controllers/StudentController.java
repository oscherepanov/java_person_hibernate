package controllers;

import GW.StudentGateway;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import models.Course;
import models.Student;
import models.University;
import org.controlsfx.control.CheckListView;
import registries.GWRegistry;

import java.io.IOException;

public class StudentController extends AController<Student, StudentGateway> {
    @FXML
    private TextField nameEdit;

    @FXML
    private VBox root;

    @FXML
    private ComboBox<University> universityCmb;

    private PassportController passportController;

    @FXML
    private CheckListView<Course> course_list;


    @FXML
    public void initialize(){
        gateway = GWRegistry.getInstance().getStudentGateway();

        universityCmb.setItems(FXCollections.observableArrayList(GWRegistry.getInstance().getUniversityGateway().all()));
        universityCmb.getSelectionModel().selectFirst();

        course_list.setItems(FXCollections.observableArrayList(GWRegistry.getInstance().getCourseGateway().all()));

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/passport_view.fxml"));
        try {
            root.getChildren().add(4, loader.load());
            this.passportController = loader.getController();
        } catch (IOException e) {
        }
    }


    @Override
    protected void toView() {
        nameEdit.setText(object.getName());
        universityCmb.getSelectionModel().select(object.getUniversity());
        passportController.setPassport(object.getPassport());
        passportController.toView();
        for(Course course: object.getCourses()){
            course_list.getCheckModel().check(course);
        }
    }

    @Override
    protected void fromView() {
        String name = nameEdit.getText();
        object.setName(name);
        object.setUniversity(universityCmb.getSelectionModel().getSelectedItem());
        passportController.fromView();
        object.getCourses().clear();
        object.getCourses().addAll(course_list.getCheckModel().getCheckedItems());
    }

    @Override
    protected Stage getStage() {
        return (Stage) nameEdit.getScene().getWindow();
    }
}

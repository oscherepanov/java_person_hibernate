package controllers;

import models.EntityClass;

public interface Controller<T extends EntityClass> {
    void setObject(T obj);
}

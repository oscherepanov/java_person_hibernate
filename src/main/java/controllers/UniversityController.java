package controllers;

import GW.UniversityGateway;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.University;
import registries.GWRegistry;

public class UniversityController extends AController<University, UniversityGateway> {
    @FXML
    private TextField nameEdit;

    @FXML
    private TextField shortNameEdit;

    public void initialize() {
        gateway = GWRegistry.getInstance().getUniversityGateway();
    }

    @Override
    protected void toView() {
        nameEdit.setText(object.getName());
        shortNameEdit.setText(object.getShortName());
    }

    @Override
    protected void fromView() {
        object.setName(nameEdit.getText());
        object.setShortName(shortNameEdit.getText());
    }

    @Override
    protected Stage getStage() {
        return (Stage) nameEdit.getScene().getWindow();
    }
}

package controllers;

import GW.CourseGateway;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.Course;
import registries.GWRegistry;

import java.io.IOException;

public class CoursesController {
    @FXML
    private TableView<Course> tableView;

    @FXML
    private TableColumn<Course, String> nameTableColumn;

    private CourseGateway courseGateway = GWRegistry.getInstance().getCourseGateway();

    ObservableList<Course> courses = FXCollections.observableArrayList(courseGateway.all());

    @FXML
    public void initialize(){
        nameTableColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        tableView.setItems(courses);
    }

    public void add(){
        Course course = new Course();
        showEditDialog(course);
        courses.add(course);
    }

    public void delete(){
        int index = tableView.getSelectionModel().getSelectedIndex();
        courseGateway.delete(courses.get(index));
        courses.remove(index);
    }

    public void edit(){
        Course course = tableView.getSelectionModel().getSelectedItem();
        showEditDialog(course);

    }

    private void showEditDialog(Course course){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/course_view.fxml"));
            VBox page = (VBox) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Редактирование");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(page, 500, 150);
            dialogStage.setScene(scene);

            CourseController controller = loader.getController();
            controller.setObject(course);

            dialogStage.showAndWait();
        }
        catch (IOException e)
        {}
    }
}

package controllers;

import GW.CourseGateway;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.Course;
import registries.GWRegistry;

public class CourseController extends AController<Course, CourseGateway>{
    @FXML
    private TextField nameEdit;

    public void initialize() {
        gateway = GWRegistry.getInstance().getCourseGateway();
    }

    @Override
    protected void toView() {
        nameEdit.setText(object.getName());
    }

    @Override
    protected void fromView() {
        object.setName(nameEdit.getText());
    }

    @Override
    protected Stage getStage() {
        return (Stage) nameEdit.getScene().getWindow();
    }
}

package controllers;

import GW.UniversityGateway;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.University;
import registries.GWRegistry;

import java.io.IOException;

public class UniversitiesController {

    @FXML
    private TableView<University> tableView;

    @FXML
    private TableColumn<University, String> nameTableColumn;

    private UniversityGateway unviersityGateway = GWRegistry.getInstance().getUniversityGateway();

    ObservableList<University> universities = FXCollections.observableArrayList(unviersityGateway.all());

    @FXML
    public void initialize(){
        nameTableColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        tableView.setItems(universities);
    }

    public void add(){
        University university = new University();
        showEditDialog(university);
        universities.add(university);
    }

    public void delete(){
        int index = tableView.getSelectionModel().getSelectedIndex();
        unviersityGateway.delete(universities.get(index));
        universities.remove(index);
    }

    public void edit(){
        University university = tableView.getSelectionModel().getSelectedItem();
        showEditDialog(university);

    }

    private void showEditDialog(University university){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/university_view.fxml"));
            VBox page = (VBox) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Редактирование");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(page, 500, 150);
            dialogStage.setScene(scene);

            UniversityController controller = loader.getController();
            controller.setObject(university);

            dialogStage.showAndWait();
        }
        catch (IOException e)
        {}
    }
}

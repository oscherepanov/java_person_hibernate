package controllers;

import GW.Gateway;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import models.EntityClass;

public abstract class AController<T extends EntityClass, G extends Gateway<T>> implements Controller<T> {
    protected T object;
    protected G gateway;

    protected abstract void toView();
    protected abstract void fromView();
    protected abstract Stage getStage();

    public void setObject(T object) {
        this.object = object;
        toView();
    }

    public void clickOk() {
        fromView();
        try {
            if (object.getId() != 0)
                gateway.update(object);
            else
                gateway.insert(object);

            Stage stage = getStage();
            stage.close();
        }
        catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }

    public void clickCancel(){
        Stage stage = getStage();
        stage.close();
    }
}

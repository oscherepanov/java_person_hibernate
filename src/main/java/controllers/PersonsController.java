package controllers;

import GW.PersonGateway;
import GW.StudentGateway;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.EntityClass;
import models.Person;
import models.Student;
import registries.GWRegistry;

import java.io.IOException;

public class PersonsController {

    static String personView = "/views/person_view.fxml";
    static String studentView = "/views/student_view.fxml";

    @FXML
    private TableView<Person> tableView;

    @FXML
    private TableColumn<Person, String> nameTableColumn;

    @FXML
    private ComboBox<String> typeCmb;

    private PersonGateway personGateway = GWRegistry.getInstance().getPersonGateway();
    private StudentGateway studentGateway = GWRegistry.getInstance().getStudentGateway();

    ObservableList<Person> persons = FXCollections.observableArrayList(personGateway.all());

    @FXML
    public void initialize(){
        nameTableColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        tableView.setItems(persons);

        typeCmb.setItems(FXCollections.observableArrayList("Person", "Student"));
        typeCmb.getSelectionModel().select("Person");
        typeCmb.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                changeType();
            }
        });
    }

    public void changeType(){
        String type = typeCmb.getSelectionModel().getSelectedItem();
        if (type.equals("Person"))
            persons.setAll(personGateway.all());
        else {
            persons.setAll(studentGateway.all());
        }
    }

    public void add(){
        String type = typeCmb.getSelectionModel().getSelectedItem();
        Person person = null;
        if (type.equals("Person")) {
            person = new Person();
            showEditDialog(person, personView);
        }
        else {
            person = new Student();
            showEditDialog(person, studentView);

        }
        persons.add(person);
    }

    public void delete(){
        String type = typeCmb.getSelectionModel().getSelectedItem();
        int index = tableView.getSelectionModel().getSelectedIndex();
        if (index != -1) {
            if (type.equals("Person"))
                personGateway.delete(persons.get(index));
            else
                studentGateway.delete((Student) persons.get(index));
            persons.remove(index);
        }
    }

    public void edit(){
        String type = typeCmb.getSelectionModel().getSelectedItem();
        Person person = tableView.getSelectionModel().getSelectedItem();
        if (type.equals("Person"))
            showEditDialog(person, personView);
        else
            showEditDialog(person, studentView);

    }

    private <T extends EntityClass> void showEditDialog(T person, String view){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource(view));
            VBox page = (VBox) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Редактирование");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            Controller<T> controller = loader.getController();
            controller.setObject(person);

            dialogStage.showAndWait();
        }
        catch (IOException e)
        {}
    }
}

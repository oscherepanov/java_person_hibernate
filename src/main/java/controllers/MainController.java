package controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class MainController {
    public void persons(){
        openDialog("/views/persons_view.fxml", "Люди");
    }

    public void universities(){
        openDialog("/views/universities_view.fxml", "Университеты");
    }

    public void courses(){
        openDialog("/views/courses_view.fxml", "Курсы");
    }

    private void openDialog(String view, String title){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource(view));
            Parent page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle(title);
            dialogStage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            dialogStage.showAndWait();
        }
        catch (IOException e)
        {}
    }
}

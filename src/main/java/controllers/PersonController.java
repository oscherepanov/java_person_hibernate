package controllers;

import GW.PersonGateway;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import models.Person;
import registries.GWRegistry;

import java.io.IOException;


public class PersonController extends AController<Person, PersonGateway> {
    private PassportController passportController;

    @FXML
    private TextField nameEdit;

    @FXML
    private VBox root;

    @FXML
    public void initialize(){
        gateway = GWRegistry.getInstance().getPersonGateway();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/passport_view.fxml"));
        try {
            root.getChildren().add(2, loader.load());
            this.passportController = loader.getController();
        } catch (IOException e) {
        }
    }

    @Override
    protected void toView() {
        nameEdit.setText(object.getName());
        passportController.setPassport(object.getPassport());
        passportController.toView();
    }

    @Override
    protected void fromView() {
        String name = nameEdit.getText();
        object.setName(name);
        passportController.fromView();
    }

    @Override
    protected Stage getStage() {
        return (Stage) nameEdit.getScene().getWindow();
    }
}

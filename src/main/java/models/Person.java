package models;

import com.sun.istack.NotNull;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;

@Entity
@Access(AccessType.PROPERTY)
@Inheritance(strategy = InheritanceType.JOINED)
public class Person implements EntityClass{

    protected int id;
    protected StringProperty name = new SimpleStringProperty();
    protected ObjectProperty<Passport> passport = new SimpleObjectProperty<>(new Passport());

    public Person()
    {}

    public Person(String name){
        this.name.set(name);
    }

    public Person(String name, Passport passport){
        this.name.set(name);
        this.passport.set(passport);
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="passport_id")
    public Passport getPassport() {
        return passport.get();
    }

    public void setPassport(Passport passport) {
        this.passport.set(passport);
    }

    @Transient
    public ObjectProperty<Passport> passportProperty() {
        return passport;
    }

    @NotNull
    public String getName() {
        return name.get();
    }

    @Transient
    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    @Id
    @GeneratedValue(generator="person_gen")
    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString(){
        return getName();
    }
}

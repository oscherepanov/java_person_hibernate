package models;

import com.sun.istack.NotNull;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;

@Entity
@Access(AccessType.PROPERTY)
public class Course implements EntityClass {
    private int id;
    private StringProperty name = new SimpleStringProperty();

    public  Course(){
    }

    public Course(String name){
        this.name.set(name);
    }

    @Id
    @GeneratedValue(generator="course_gen")
    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name.get();
    }

    @Transient
    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    @Override
    public String toString(){
        return name.get();
    }

    @Override
    public boolean equals(final Object other){
        if (this == other)
            return true;
        if (!(other instanceof Course))
            return false;
        return this.id == ((Course) other).getId();
    }
}

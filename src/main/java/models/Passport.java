package models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;

@Entity
@Access(AccessType.PROPERTY)
public class Passport implements EntityClass{
    private int id;
    private StringProperty series = new SimpleStringProperty();
    private StringProperty number = new SimpleStringProperty();

    @Id
    @GeneratedValue(generator="passport_gen")
    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSeries() {
        return series.get();
    }

    @Transient
    public StringProperty seriesProperty() {
        return series;
    }

    public void setSeries(String series) {
        this.series.set(series);
    }

    public String getNumber() {
        return number.get();
    }

    @Transient
    public StringProperty numberProperty() {
        return number;
    }

    public void setNumber(String number) {
        this.number.set(number);
    }
}

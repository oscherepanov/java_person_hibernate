package models;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Access(AccessType.PROPERTY)
public class Student extends Person{
    protected ObjectProperty<University> university = new SimpleObjectProperty<>();

    @Access(AccessType.FIELD)
    @ManyToMany
    @JoinTable(name="STUDENT_COURSE", joinColumns = @JoinColumn(name = "STUDENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "COURSE_ID"))
    protected Set<Course> courses = new HashSet<>();

    public Student(){
        super();
    }

    public Student(String name, Passport passport, University university){
        super(name, passport);
        this.university.set(university);
    }

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name="university_id", nullable = false)
    public University getUniversity() {
        return university.get();
    }

    @Transient
    public ObjectProperty<University> universityProperty() {
        return university;
    }

    public void setUniversity(University university) {
        this.university.set(university);
    }

    public Set<Course> getCourses() {
        return courses;
    }
}

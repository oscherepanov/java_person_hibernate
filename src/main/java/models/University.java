package models;

import com.sun.istack.NotNull;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;

@Entity
@Access(AccessType.PROPERTY)
public class University implements EntityClass {
    private int id;
    private StringProperty name = new SimpleStringProperty();
    private StringProperty shortName = new SimpleStringProperty();

    public University(){
    }

    public University(String name, String shortName){
        this.name.set(name);
        this.shortName.set(shortName);
    }

    @Id
    @GeneratedValue(generator="univer_gen")
    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name.get();
    }

    @Transient
    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    @NotNull
    public String getShortName() {
        return shortName.get();
    }

    @Transient
    public StringProperty shortNameProperty() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName.set(shortName);
    }

    @Override
    public String toString(){
        return shortName.get();
    }
}

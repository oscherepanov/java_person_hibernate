package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/main.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Главное окно");
        primaryStage.setScene(new Scene(root, 300, 120));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

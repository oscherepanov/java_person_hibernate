package registries;

import GW.CourseGateway;
import GW.PersonGateway;
import GW.StudentGateway;
import GW.UniversityGateway;

public class GWRegistry {
    private PersonGateway personGateway = new PersonGateway();
    private StudentGateway studentGateway = new StudentGateway();
    private UniversityGateway universityGateway = new UniversityGateway();
    private CourseGateway courseGateway = new CourseGateway();

    private static GWRegistry instance = new GWRegistry();

    private GWRegistry() {
    }

    public static GWRegistry getInstance() {
        return instance;
    }

    public PersonGateway getPersonGateway() {
        return personGateway;
    }

    public StudentGateway getStudentGateway() {
        return studentGateway;
    }

    public UniversityGateway getUniversityGateway(){
        return universityGateway;
    }

    public CourseGateway getCourseGateway(){
        return courseGateway;
    }
}